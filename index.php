<?php

/** @noinspection PhpUndefinedClassInspection */

// Nur Parser Fehler anzeigen
error_reporting(E_PARSE);

/**
 * Class KEMInfo
 *
 * Liest Versionen und Informationen zu Server und Software aus und gibt diese als JSON zurück.
 * Das Script kann sich selbst aktualisieren.
 */
class KEMInfo
{
    const VERSION = '1.22.2';
    protected $response = array();
    protected $public_key = '';
    protected $extensions = array();

    public function __construct($public_key)
    {
        $this->public_key = $public_key;

        register_shutdown_function(array('KEMInfo', 'shutDownFunction'));
    }

    /**
     * PHP Fatal Errors abfangen und ausgeben
     */
    public static function shutDownFunction()
    {
        if (null !== ($error = error_get_last()) && $error['type'] === E_ERROR)
        {
            self::sendError('PHP Fatal Error', array('php-error' => $error));
        }
    }

    /**
     * Startet die Anwendung
     */
    public function execute()
    {
        try
        {
            // Update ausführen
            if (isset($_POST['update']))
            {
                $this->update();
            }

            // Basis Antwort Daten
            $this->response = array(
                'success' => true,
                'version' => self::VERSION
            );

            // Installation prüfen
            $this->selfCheck();

            // Anfrage validieren
            if (PHP_SAPI !== 'cli')
            {
                $this->validateRequest();
            }

            // Infos sammeln
            /** @var KEMInfoExtension $extension */
            foreach ($this->extensions as $extension)
            {
                try
                {
                    $extension->extensions = $this->response;
                    $data = $extension->execute();
                    if ($data !== null)
                    {
                        $this->response[$extension->getName()] = $data;
                    }
                }
                catch (Exception $e)
                {
                    $this->addError($extension->getName(), $e->getMessage() . '. Trace: ' . $e->getTraceAsString());
                }
            }

            // Daten als JSON zurückgeben
            self::sendResponse($this->response);
        }
        catch (Exception $e)
        {
            $this->addErrorAndSendResponse('main','Exception: ' . $e->getMessage());
        }
    }

    /**
     * Fügt eine Erweiterung zum Sammeln von Daten hinzu
     * @param $extension KEMInfoExtension
     */
    public function addExtension($extension)
    {
        $this->extensions[] = $extension;
    }

    /**
     * Prüft ob die Daten korrekt signiert sind.
     *
     * @param $data string
     * @param $signature_base64 string base64 encoded signature
     * @return boolean
     */
    protected function checkSignature($data, $signature_base64)
    {
        $signature_sha512 = base64_decode($signature_base64);
        $valid = openssl_verify($data, $signature_sha512, $this->public_key, 'sha512WithRSAEncryption');

        // openssl Fehler
        if ($valid === -1)
        {
            $this->addErrorAndSendResponse('main','Signature error: ' . openssl_error_string());
        }

        return ($valid === 1);
    }

    /**
     * Prüfen ob die Anfrage gültig ist
     */
    protected function validateRequest()
    {
        // Anfrage ohne Parameter => Testaufruf nach Installation
        if (!isset($_REQUEST['host']))
        {
            // Antwort senden nur mit self-check
            $this->addErrorAndSendResponse('validation','Missing validation parameter.', $this->response);
        }

        // Host prüfen
        $localhost = (!empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']);
        if (empty($_REQUEST['host']) || $_REQUEST['host'] !== $localhost)
        {
            $this->addErrorAndSendResponse('validation','Host does not match.',
                array('host' => htmlentities($localhost))
            );
        }

        // Zeit darf nur um 5 Minuten abweichen
        if (empty($_REQUEST['date']) || (time() - (int)$_REQUEST['date']) > 5*60)
        {
            $this->addErrorAndSendResponse('validation','Date does not match',
                array('time' => time())
            );
        }

        if (!$this->checkSignature($localhost . $_REQUEST['date'], $_REQUEST['request_signature_sha512']))
        {
            $this->addErrorAndSendResponse('validation','Request signature does not match.');
        }
    }

    /**
     * Dieses Script aktualisieren
     */
    protected function update()
    {
        $update = $_POST['update'];

        // Validierung des Updates
        if (!$this->checkSignature($update, $_POST['signature_sha512']))
        {
            $this->addErrorAndSendResponse('update','Signature does not match.');
        }

        // Temprärer Name des Updates
        $updateTempName = 'index.update.php';
        if (isset($_POST['update-temp-name']))
        {
            $updateTempName = $_POST['update-temp-name'];
        }

        // Update speichern
        if (file_put_contents($updateTempName, $_POST['update']) === false)
        {
            $this->addErrorAndSendResponse('update','Upload failed.');
        }

        // Neue Version soll erst getestet werden
        if (isset($_POST['test-update']))
        {
            // Test Update löschen
            if (isset($_POST['update-remove-test']))
            {
                unlink($updateTempName);
            }

            self::sendResponse(array(
                'success' => true,
                'test-update' => true,
            ));
        }

        // Script aktualisieren
        if (!rename('index.update.php', 'index.php'))
        {
            $this->addErrorAndSendResponse('update','Rename failed.');
        }

        self::sendResponse(array(
            'success' => true
        ));
    }

    /**
     * In der Ausgabe für ein Modul einen Fehler hinzufügen.
     *
     * @param $module
     * @param $message
     */
    protected function addError($module, $message)
    {
        $this->response['errors'][$module][] = $message;
    }

    protected function addErrorAndSendResponse($module, $message, $data = array())
    {
        $this->addError($module, $message);
        self::sendError($message, array_merge($this->response, $data));
    }

    /**
     * @param $message
     * @param array $data
     */
    protected static function sendError($message, $data = array())
    {
        self::sendResponse(array_merge($data, array(
            'success' => false,
            'version' => self::VERSION,
            'error' => $message
        )));
    }

    /**
     * JSON Antwort senden
     * @param $data
     */
    protected static function sendResponse($data)
    {
        header('Content-type:application/json;charset=utf-8');
        $options = (defined('JSON_PRETTY_PRINT') ? JSON_PRETTY_PRINT : null);
        echo json_encode( $data, $options );
        if (!defined('TEST_RUN'))
        {
            exit();
        }
    }

    /******* Infos auslese **********/

    /**
     * Selbsttest durchführen: Schreibrechte, Zertifikat, ...
     */
    protected function selfCheck()
    {
        $this->response['self-check'] = true;

        // index.php schreibbar?
        if (!is_writable(__FILE__))
        {
            $this->response['self-check'] = false;
            $this->addError('self-check', 'File is not writeable');
        }

        // Verzeichnis schreibbar (für index.update.php)
        if (!is_writable(__DIR__))
        {
            $this->response['self-check'] = false;
            $this->addError('self-check', 'Directory not writeable');
        }

        if (!extension_loaded('openssl'))
        {
            $this->response['self-check'] = false;
            $this->addError('self-check', 'Missing PHP extension openssl');
        }

        if (!function_exists('openssl_verify'))
        {
            $this->response['self-check'] = false;
            $this->addError('self-check', 'Missing PHP function openssl_verify');
        }

        // Public Zertifikat ist nun hier mit drin
        if (file_exists('public_key.pem'))
        {
            unlink('public_key.pem');
        }

        if (!$this->response['self-check'])
        {
            $this->addErrorAndSendResponse('self-check', 'self-check failed');
        }
    }
}

/**
 * Method to execute a command in the terminal
 * Uses :
 * 1. system
 * 2. passthru
 * 3. exec
 * 4. shell_exec
 *
 * Source :https://www.binarytides.com/execute-shell-commands-php/
 *
 * @param $command
 * @return false|string|null
 */
function terminal($command)
{
    //system
    if(function_exists('system'))
    {
        ob_start();
        system($command);
        $output = ob_get_clean();
    }
    //passthru
    else if(function_exists('passthru'))
    {
        ob_start();
        passthru($command);
        $output = ob_get_clean();
    }

    //exec
    else if(function_exists('exec'))
    {
        exec($command , $output);
        $output = implode("\n", $output);
    }

    //shell_exec
    else if(function_exists('shell_exec'))
    {
        $output = shell_exec($command) ;
    }

    else
    {
        $output = false;
    }

    return $output;
}


/**
 * Class KEMInfoExtension (KIE)
 * Erweiterung zur Abfrage von Informationen
 */
abstract class KEMInfoExtension
{

    /**
     * Daten der anderen Erweiterungen
     * @var array
     */
    public $extensions;

    /**
     * Name der Erweiterung.
     * Bitte in Kleinbuchstaben mit Bindestrich als Trennzeichen
     *
     * @return string
     */
    abstract public function getName();

    /*
     * Sammelt Informationen und gibt diese als String oder Array zurück.
     *
     * @return array|string
     */
    abstract public function execute();
}

// Public Key
$public_key = <<<EOT
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArlPAV5CENZ3BwEqHdv1u
vEPU67e8kXwykFeox12fuLEwpiwf4ACLJSDPsszYv2tAKew6n9TEIqARe3Pe9U8w
yVHeuHVmpEzjcjtdSK98h8f7IIjLA61+18NHewshE6bDRKzVmtGm5HHsy0njXRU+
DOwvzx1xJO4w7XCAv/91tN5/Gi3oxTiZso8O9dQt5K9zSuFDz8Sp669FX6t+tuen
U7gKXuKgT87Mt6rfgV4ymzBTaaVuUp23eKaHinbBX449/YsEs5wbz63hv47xNGuR
/kN20+V+y6iJwAz+3d4f2lFY9oxAnH3nfYjGqNvumjMv2qy3yeHvmCEdycq5XadP
kwIDAQAB
-----END PUBLIC KEY-----
EOT;

$info = new KEMInfo($public_key);

//---------------------------------------------[ Erweiterungen ]--------------------------------------------------------

class KIEPhpVersion extends KEMInfoExtension
{
    public function getName()
    {
        return 'php-version';
    }

    public function execute()
    {
        return PHP_VERSION;
    }
}
$info->addExtension(new KIEPhpVersion());

// Server Info
class KIEServer extends KEMInfoExtension
{
    public function getName()
    {
        return 'server';
    }

    public function execute()
    {
        $result = array(
            'webroot' => $_SERVER['DOCUMENT_ROOT'],
            'ip' => $_SERVER['SERVER_ADDR'],
            'name' => $_SERVER['SERVER_NAME'],
            'httpd' => $_SERVER['SERVER_SOFTWARE'],
        );

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $result['HTTP_X_FORWARDED_FOR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        // Linux Distribution und Version
        if (file_exists('/proc/version'))
        {
            $version = file_get_contents('/proc/version');
            if (!empty($version))
            {
                $result['linux']['proc-version'] = $version;
            }
        }

        if (file_exists('/etc/os-release'))
        {
            $distribution = file_get_contents('/etc/os-release');
            if (!empty($distribution))
            {
                $result['linux']['os-release'] = $distribution;
            }
        }

        $result['linux']['lsb_release'] = terminal('lsb_release -a');
        $result['linux']['hostnamectl'] = terminal('hostnamectl');
        $result['linux']['uname'] = terminal('uname -r');

        return $result;
    }
}
$info->addExtension(new KIEServer());

// composer
class KIEComposer extends KEMInfoExtension
{
    public function getName()
    {
        return 'composer';
    }

    public function execute()
    {
        $composerFile = $this->getComposer();

        if ($composerFile === null)
        {
            return null;
        }

        $composer_file = file_get_contents($composerFile);
        $composer = json_decode($composer_file, true);

        $packages = array();
        if (!empty($composer['packages']))
        {
            foreach ($composer['packages'] as $package)
            {
                $packages[ $package['name'] ] = [
                    'version' => $package['version'],
                    'time' => $package['time'],
                ];
            }
        }

        return $packages;
    }

    protected function getComposer()
    {
        $files = [
            '../composer.lock',
            '../../composer.lock',
            '../../../composer.lock',
        ];

        foreach ($files as $file)
        {
            if (file_exists($file))
            {
                return $file;
            }
        }

        return null;
    }
}
$info->addExtension(new KIEComposer());

// Typo3
class KIETypo3 extends KEMInfoExtension
{
    public function getName()
    {
        return 'typo3';
    }

    public function execute()
    {
        // älteres Typo3 mit symlink
        if (is_link('../typo3_src'))
        {
            return $this->getVersionFromSymLink();
        }

        // Typo3 mit Composer
        if (isset($this->extensions['composer']['typo3/cms-core']))
        {
            return $this->getVersionFromComposer();
        }

        // Typo3 Dateien liegen außerhalb des Webroots
        if ($this->versionFileExists($this->extensions['server']['webroot'] . '/..'))
        {
           return $this->getVersionFile();
        }

        return null;
    }

    protected function getVersionFromSymLink()
    {
        $link = readlink('../typo3_src');
        $link = basename($link);
        $parts = explode('-', $link);

        $typo3 = null;

        if (count($parts) === 3)
        {
            $typo3 = array(
                'licence' => $parts[1],
                'version' => $parts[2]
            );
        }
        else if (count($parts) === 2)
        {
            $typo3 = array(
                'version' => $parts[1]
            );
        }

        return $typo3;
    }

    protected function getVersionFromComposer()
    {
        $version = $this->extensions['composer']['typo3/cms-core']['version'];

        if (!empty($version) && $version[0] === 'v')
        {
            $version = substr($version, 1);
        }

        if (file_exists('../../auth.json'))
        {
            return array(
                'licence' => 'elts',
                'version' => $version
            );
        }

        return array(
            'version' => $version
        );
    }

    protected function versionFileExists($path)
    {
        if (file_exists($path . '/private/typo3/sysext/core/Classes/Information/Typo3Version.php'))
        {
            // TODO ELTS Version

            /** @noinspection PhpIncludeInspection */
            require $path . '/private/typo3/sysext/core/Classes/Information/Typo3Version.php';
            if (class_exists('\TYPO3\CMS\Core\Information\Typo3Version'))
            {
                return true;
            }
        }

        return false;
    }

    protected function getVersionFile()
    {
        /** @noinspection PhpUndefinedNamespaceInspection */
        $typoVersion = new \TYPO3\CMS\Core\Information\Typo3Version();

        return array(
            'version' => $typoVersion->getVersion()
        );
    }
}
$info->addExtension(new KIETypo3());

// Magento
class KIEMagento extends KEMInfoExtension
{
    public function getName()
    {
        return 'magento';
    }

    public function execute()
    {
        if (file_exists('../app/Mage.php'))
        {
            /** @noinspection PhpIncludeInspection */
            require '../app/Mage.php';

            /** @noinspection PhpUndefinedClassInspection */
            return array(
                'version' => Mage::getVersion()
            );
        }

        return null;
    }
}
$info->addExtension(new KIEMagento());

// Shopware
class KIEShopware extends KEMInfoExtension
{
    public function getName()
    {
        return 'shopware';
    }

    public function execute()
    {
        if (is_dir('../engine/Shopware'))
        {
            /** @noinspection PhpIncludeInspection */
            require '../autoload.php';

            // Shopware < v5.5
            if (defined('Shopware::VERSION'))
            {
                /** @noinspection PhpUndefinedClassInspection */
                return array(
                    'version' => Shopware::VERSION
                );
            }

            // Shopware >= v5.5
            /** @noinspection PhpFullyQualifiedNameUsageInspection */
            /** @noinspection PhpUndefinedClassInspection */
            /** @noinspection PhpUndefinedNamespaceInspection */
            $kernel = new \Shopware\Kernel('testing', true);
            $kernel->boot();

            return array(
                'version' => $kernel->getContainer()->get('config')->get('version')
            );
        }

        return null;
    }
}
$info->addExtension(new KIEShopware());

// Joomla
class KIEJoomla extends KEMInfoExtension
{
    public function getName()
    {
        return 'joomla';
    }

    public function execute()
    {
        // Quelle: https://www.itoctopus.com/how-to-quickly-know-the-version-of-any-joomla-website
        // Datei mit Version finden
        $file = false;
        $versionFiles = array(
            // >= 1.6.0
            '../administrator/manifests/files/joomla.xml',
            //  >= 1.5.0 and <= 1.5.26
            '../language/en-GB/en-GB.xml',
            // < 1.5.0
            '../modules/custom.xml'
        );
        foreach ($versionFiles as $versionFile)
        {
            if (file_exists($versionFile))
            {
                $file = file_get_contents($versionFile);
                break;
            }
        }

        // Version auslesen
        if ($file)
        {
            if (function_exists('simplexml_load_string'))
            {
                $xml = simplexml_load_string($file);
                $version = isset($xml->version[0]) ? (string) $xml->version : null;
            }
            else
            {
                $s = substr($file, strpos($file, '<version>') + 9);
                $version = substr($s, 0, strpos($s, '</version>'));
            }

            if (!empty($version))
            {
                // Versionen <= 1.5.26 können nicht genau ermittelt werden, deswegen kürzen wir hier die Version.
                $mainVersion = substr($version, 0, 3);
                if ($mainVersion === '1.0')
                {
                    $version = '1.0';
                }
                else if ($mainVersion === '1.5')
                {
                    $version = '1.5';
                }

                return array(
                    'version' => $version
                );
            }
        }

        return null;
    }
}
$info->addExtension(new KIEJoomla());

// Server Check
class KIEServerCheck extends KEMInfoExtension
{
    public function getName()
    {
        return 'server-check';
    }

    public function execute()
    {
        return array(
            // Festplatten Speicher prüfen
            'diskspace' => $this->checkDiskSpace(),
            // Datenbank prüfen
            'mysql' => $this->checkDatabase(),
            // Average Load
            'average-load' => $this->getAverageLoad(),
            // CPUs
            'cpus' => $this->getCPUs(),
        );
    }

    /**
     * Prüft den Platz auf der Festplatte.
     *  web: freier Speicher im Verzeichnis in dem keminfo liegt.
     *  temp: freier Speicher im Temp Verzeichnis des Systems (/tmp)
     *  folders: Wenn möglich wird mit `df` die gemounteten Verzeichnisse ausgelesen mit jeweil 'total', 'free' und 'percent'
     *
     * @return array
     */
    protected function checkDiskSpace()
    {
        $response = array(
            'dir' => __DIR__,
            'web' => disk_free_space(__DIR__),
            'temp' => disk_free_space(sys_get_temp_dir())
        );

        // Laufwerke ohne tempfs, loopback, ...
        $out = terminal('df -x squashfs -x tmpfs -x devtmpfs');

        $lines = explode("\n", $out);
        if (is_array($lines))
        {
            array_shift($lines);
            foreach($lines as $line)
            {
                // Prozent und Verzeichnis auslesen
                preg_match_all("/(\d+)\s+(\d+)\s+(\d+)%\s(.*)/",$line,$matches);
                if (isset($matches[1][0], $matches[2][0], $matches[3][0], $matches[4][0]))
                {
                    $total = $matches[1][0];
                    $free = $matches[2][0];
                    $prozent = $matches[3][0];
                    $folder = $matches[4][0];
                    $response['folders'][$folder] = array(
                        'total' => $total,
                        'free' => $free,
                        'percent' => $prozent,
                    );
                }
            }
        }

        return $response;
    }

    /**
     * Prüft ob der Datenbank Server reagiert.
     * Dazu wird ohne Benutzer und Passwort auf localhost und 127.0.0.1 verbunden.
     * Als korrekte Antwort wird der Fehler 1045 (Access denied) erwartet => true.
     * Alles andere wird als Problem gewertet => false.
     *
     * @return array|null
     */
    protected function checkDatabase()
    {
        $db_error_socket = false;
        $db_error_ip = false;

        // Teste Datenbank Verbindung mit verschiedenen Treibern und gibt Fehlercode zurück.
        if (class_exists('MySQLi'))
        {
            mysqli_report(MYSQLI_REPORT_OFF);

            $h = new MySQLi('localhost');
            $db_error_socket = $h->connect_errno;

            $h = new MySQLi('127.0.0.1');
            $db_error_ip = $h->connect_errno;
        }
        elseif (class_exists('PDO'))
        {
            $pdo = new PDO('localhost', null, null, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT));
            $db_error_socket = $pdo->errorCode();

            $pdo = new PDO('127.0.0.1', null, null, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT));
            $db_error_ip = $pdo->errorCode();
        }
        elseif (function_exists('mysql_connect'))
        {
            /** @noinspection PhpDeprecationInspection */
            $link = mysql_connect('localhost');
            if (!$link)
            {
                /** @noinspection PhpDeprecationInspection */
                $db_error_socket = mysql_errno();
            }

            /** @noinspection PhpDeprecationInspection */
            $link = mysql_connect('127.0.0.1');
            if (!$link)
            {
                /** @noinspection PhpDeprecationInspection */
                $db_error_ip = mysql_errno();
            }
        }
        else
        {
            //'ERROR: Missing mysql driver!';
            return null;
        }

        // 1045 Access denied = Server läuft und reagiert
        return array(
            'running' => ($db_error_socket === 1045) || ($db_error_ip === 1045)
        );
    }

    protected function getAverageLoad()
    {
        // average load
        // cat /proc/loadavg
        $out = terminal('cat /proc/loadavg');
        $loadavg = explode(' ', $out, 4);
        if (is_array($loadavg) && count($loadavg) === 4)
        {
            array_pop($loadavg);
            return $loadavg;
        }

        // uptime
        $out = terminal('uptime');
        $i = strpos($out, 'load average:');
        if ($i !== false)
        {
            $load = substr($out, $i + strlen('load average:'), -1);
            $load = trim(str_replace(', ', ' ', $load));
            $load = str_replace(',', '.', $load);
            return explode(' ', $load);
        }

        // w
        #$out = terminal('w');

        return null;
    }

    protected function getCPUs()
    {
        return terminal('nproc');
    }
}
$info->addExtension(new KIEServerCheck());

// Wordpress
class KIEWordpress extends KEMInfoExtension
{
    public function getName()
    {
        return 'wordpress';
    }

    public function execute()
    {
        if (file_exists('../wp-includes/version.php'))
        {
            /** @noinspection PhpIncludeInspection */
            require '../wp-includes/version.php';

            /** @noinspection IssetArgumentExistenceInspection */
            return array(
                'version' => isset($wp_version) ? $wp_version : null
            );
        }
        if (file_exists('../wordpress/wp-includes/version.php'))
        {
            /** @noinspection PhpIncludeInspection */
            require '../wordpress/wp-includes/version.php';

            /** @noinspection IssetArgumentExistenceInspection */
            return array(
                'version' => isset($wp_version) ? $wp_version : null
            );
        }

        return null;
    }
}
$info->addExtension(new KIEWordpress());

class KIEEnv extends KEMInfoExtension
{
    public function getName()
    {
        return 'env';
    }

    public function execute()
    {
        $envPath = '../../.env';

        if (file_exists($envPath) && is_readable($envPath))
        {
            $env = explode("\n", file_get_contents($envPath));
            $values = array();

            foreach ($env as $line)
            {
                if ((strpos($line, 'APP_DEBUG=') === 0)
                    || (strpos($line, 'APP_ENV=') === 0)
                    || (strpos($line, 'APP_NAME=') === 0)
                    || (strpos($line, 'APP_URL=') === 0)
                )
                {
                    $values[] = $line;
                }
            }

            return array(
                'values' => $values
            );
        }

        return null;
    }
}
$info->addExtension(new KIEEnv());

// Hier bitte Erweiterungen einfügen
//----------------------------------------------------------------------------------------------------------------------

// Anwendung starten
$info->execute();