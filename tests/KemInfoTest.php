<?php
use PHPUnit\Framework\TestCase;

/**
 * starten mit:
 * ./vendor/bin/phpunit tests
 *
 * Doku: https://phpunit.de/getting-started/phpunit-9.html
 */

final class KemInfoTest extends TestCase
{
    public function testStart()
    {
        ob_start();
        define('TEST_RUN', true);
        include 'index.php';
        $response = ob_get_clean();

        // Basis Daten prüfen
        $this->assertJson( $response );
        $data = json_decode($response, true);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('success', $data);
        $this->assertTrue($data['success']);
        $this->assertArrayHasKey('version', $data);
        $this->assertArrayHasKey('self-check', $data);
        $this->assertTrue($data['self-check']);

        // TODO error feedback?
        // TODO self-check

        // TODO validateRequest
        // TODO extensions
    }
}