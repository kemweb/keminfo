# KEMInfo

`index.php` gibt Infos über den Server, Software und verwendete Versionen als JSON aus.

Außerdem kann sich das Script selbst aktualisieren. Das Update wird per POST übertragen.

Aus Sicherheitsgründen sind alle Anfragen und Updates mit Privat-Public-Key Verfahren (RSA) signiert.
Unser Public-Key `public_key.pem` ist bereits im Projekt enthalten.

## Installation auf Server
Für root anlegen `su` oder `sudo -s`:

    cd ~
    git clone https://bitbucket.org/kemweb/keminfo.git
    cd keminfo
    chmod +x install-server.sh

Crontab erstellen `crontab -e`

    0 10 * * * /root/keminfo/install-server.sh

Done:
* 11 kemservice.de
* 30 kembase
* 31 kemglobe.de
* 07 kemspace.de (schein offline zu sein)

Todo:
* kemworker https://forge.laravel.com/servers/455315/sites
* lesestart-db https://forge.laravel.com/servers/457203/sites
* 35 kempire.net: ist nicht mehr viel drauf
        
       error: gnutls_handshake() failed: A TLS fatal alert has been received.
       => updates installieren => apt-get update: stretch Release' does no longer have a Release file

## Installation in einem Webroot
* Im Webroot (ggf. public) ein Verzeichnis keminfo anlegen.
* `index.php` hochladen.
* Webserver Schreibrechte auf `/keminfo` und `/keminfo/index.php` geben,  bei Laravel `/public/keminfo`.
* Ruft man die Seite nach der Installation auf gibt die Anwendung zwar einen Fehler aus.
Bei `"self-check":true` ist jedoch alles korrekt installiert.

    `{"success":false,"version":"0.8","self-check":true,"error":"Missing validation parameter."}`
    
* Prüfen ob die Domain in Scoptimo eingetragen ist: https://de.scoptimo.de/domaininfo 
    wenn nicht bitte an Nico melden.
* Unter https://de.scoptimo.de/domaininfo, auf die neue Domain und "Re-Check" klicken.

Script

    mkdir keminfo
    cd keminfo
    wget https://bitbucket.org/kemweb/keminfo/raw/1c921f215ed639009ef98a5af22edb3b609a5123/index.php
    chmod a+w -R .
    php index.php
    php index.php | grep "self-check"

## Entwicklung weitere Info Module

Im Script ganz unten vor `$info->execute();` können Erweiterungen in dieser Form eingefügt werden:

    class KIEPhpVersion extends KEMInfoExtension
    {
        /**
         * Name der Erweiterung.
         * Bitte in Kleinbuchstaben mit Bindestrich als Trennzeichen
         *
         * @return string
         */
        public function getName()
        {
            return 'php-version';
        }
    
        /*
         * Sammelt Informationen und gibt diese als String oder Array zurück.
         *
         * @return array|string
         */
        public function execute()
        {
            return PHP_VERSION;
        }
    }
    $info->addExtension(new KIEPhpVersion());

## TODOs
* Die Erweiterungen sollten in separate Dateien ausgelagert werden, die dann in eine Datei "compiliert" werden.
  Adminer macht das: https://github.com/vrana/adminer/blob/master/compile.php
  https://github.com/codeless/JuggleCode
* Ein Austausch des Schlüssels sollte vorgesehen werden