#!/bin/bash
echo "# KEMInfo"
echo "## Downloading new version..."
git reset --hard HEAD
git pull
chmod +x install-server.sh

# get all DocumentRoot
# apache/nginx ?
if test -d "/etc/nginx/"; then
  echo "Found nginx"
  paths=$(grep -h -i 'root' /etc/nginx/sites-enabled/* | sed -E 's/.*root "?(.*?)"?/\1/' | sed -E 's/;//g' | sort | uniq)
else
  echo "Found Apache"
  # grep DocumentRoots, grep path, remove "", skip path with %1, sort, unique
  paths=$(grep -h -i 'DocumentRoot' /etc/apache2/sites-enabled/* | sed -E 's/.*DocumentRoot (.*)/\1/' | sed -E 's/"//g' | grep -v "%" | sort | uniq)
fi

echo "## Copy to all webspaces..."
for webroot in $paths; do
  echo "Updating: $webroot"

  if [ ! -d $webroot ]; then
    echo "ERROR webroot '$webroot' does not exist!"
    continue
  fi

  keminfo="$webroot/keminfo"

  # remove symlink
  if [[ -L "$keminfo" ]]; then
    echo "Delete symlink"
    ls -la "$keminfo"
    rm -rf "$keminfo"
  fi

  # create
  mkdir -p "$keminfo"
  cp -f "index.php" "$keminfo"

  # Set owner
  owner=$(stat -c "%U" "$webroot")
  group=$(stat -c "%G" "$webroot")
  chown -R "$owner":"$group" "$keminfo"
  ls -la "$keminfo"
done

echo "All done."
